import com.*; import com.softsynth.*; import com.softsynth.jsyn.*; import com.softsynth.jsyn.view102.*; import com.softsynth.jsyn.util.*; import com.softsynth.jsyn.view11x.*; import com.softsynth.jsyn.view.*; import com.softsynth.jsyn.circuits.*; import com.softsynth.util.*; import com.softsynth.view.*; import com.softsynth.math.*; import com.*; import com.softsynth.*; import com.softsynth.jsyn.*; import java.applet.*; import java.awt.*; import java.awt.image.*; import java.awt.event.*; import java.io.*; import java.net.*; import java.text.*; import java.util.*; import java.util.zip.*; import netscape.javascript.*; import javax.comm.*; import javax.sound.midi.*; import javax.sound.midi.spi.*; import javax.sound.sampled.*; import javax.sound.sampled.spi.*; import javax.xml.parsers.*; import javax.xml.transform.*; import javax.xml.transform.dom.*; import javax.xml.transform.sax.*; import javax.xml.transform.stream.*; import org.xml.sax.*; import org.xml.sax.ext.*; import org.xml.sax.helpers.*; public class sonia_chaotic02 extends BApplet {BFont f;
int tMin, tSec, tHour, tMS;
int offset, initSec;
boolean checkOffset;
int MAXenv = 512;
float env[] = new float[MAXenv];
Sample mySample;
//-------------------------------
float x1 = 0.5f;
float y1 = 0.5f;
float x2, y2;
//-------------------------------

void mousePressed(){
  mySample.repeat();
}
void mouseReleased(){
  //mySample.stop();
}
void setPan(int n){
  float pan = -1f + n/(width/2f);
  mySample.setPan(pan);
}

void setVolume(int n){
  // Range: float from 0 to 1
  float vol = n/(height*1f);
  mySample.setVolume(vol);
}
void setRate(int n){
  // Values:
  // 0 -> very low pitch (slow playback).
  // 88200 -> very high pitch (fast playback).
  float rate = (height - n)*88200/(height);
  mySample.setRate(rate);
}

// Draw a scroller that shows the current sample-frame being played.
// Notice how the sample plays faster when the Sample-Rate is higher.(controlled by mouseY)

void drawScroller(){
  strokeWeight(1);
  stroke(0,255,0);
  // figure out which percent of the sample has been played.
  float percent = mySample.getCurrentFrame() *100f / mySample.getNumFrames();
  // calculate the marker position
  float marker = percent*width/100f;
  // draw...
  line(marker,0,marker,20);
  line(0,10, width,10);
}

//---------------------------------------------------------------
void setup(){
  size(512,200);
  f = loadFont("Courier_New.vlw");
  textFont(f, 30);
  offset = 0;
  initSec = second();
  checkOffset = true;
  //----------------------
  Sonia.start(this); // Start Sonia engine.
  // create a new sample object.
  mySample = new Sample("Sound 3.aif");
  rectMode(CENTER_DIAMETER);

  for(int i = 0; i < MAXenv; i++){
    float oneCycle = i/(float)MAXenv*TWO_PI;
    float phase = PI/2.0f;
    env[i] =( 1+ sin(oneCycle + phase)) / 2.0f; // produce curve numbers between 0 and 1.
    //print(env[i] + " . ");
  }
}

int x;
//----------------------------------------------------------------------

void loop(){
  background(223, 228, 194);
  tHour = hour();
  tMin = minute();
  tSec = second() ;
  tMS = (millis()- offset)%999;

  if(checkOffset){
    if( initSec != tSec){
      offset = millis();
      checkOffset = false;
    }
  }
  //
  strokeWeight(1);
  // draw the curve
  for(int i = 0; i < MAXenv; i++){
    float y = 100 + env[i] * tHour;
    stroke(0, 200, 0);
    point(i, y);
  }
  //
  for (int n = 0; n < MAXenv-12; n++){
    x2 = env[n] + env[n+1]*x1 + sq(env[n+2]*x1) + env[n+3]*x1*y1 + env[n+4]*y1 + sq(env[n+5]*y1);
    y2 = env[n+6] + env[n+7]*x1 + sq(env[n+8]*x1) + env[n+9]*x1*y1 + env[n+10]*y1 + sq(env[n+11]*y1);
    stroke(255, 217, 222);
    point(n, 30+x2*tMin);
    stroke(55, 216, 234);
    point(n, 30+y2*tSec);
  //}
  //
  // If sample is playing (or looping), do this...
  if(! mySample.isPlaying()){
    mySample.play();
  }else{
    // use the curve data to control the x position on the rect.
    stroke(209, 24, 75);
    rect( env[x]*MAXenv, MAXenv%tHour+y2*50 , tMin, tSec);
    stroke(215, 242, 196);
    rect( x2*MAXenv, MAXenv%tHour+y2*100, tSec, tMin);
    x++;
    x%=MAXenv;
  }

  setRate(tMS*(int)(x2*100)); // use tMS to control sample-rate playback
  setPan(tMin*100); // use tMin to control sample Panning
  setVolume(tSec*(int)(y2*100)); // use tSec to control sample volume
}
drawScroller();
String time = nf(tHour,2) + ":" + nf(tMin,2) + ":" + nf(tSec,2) + ":" + nf(tMS,3) ;
text(time, 10, 190);
}
//----------------------------------------------------------------
// Safely close the sound engine upon Browser shutdown.
public void stop(){
Sonia.stop();
super.stop();
}
}