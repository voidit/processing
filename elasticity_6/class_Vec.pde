class Vec
{
  float x,y,z;

  Vec(float X, float Y, float Z)
  {
    x=X;
    y=Y;
    z=Z;
  }

  void add(Vec v)
  {
    x+=v.x;
    y+=v.y;
    z+=v.z;
  }

  void subtract(Vec v)
  {
    x-=v.x;
    y-=v.y;
    z-=v.z;
  }

  // multiply function, divide by using number below 1
  void multiply(float m)
  {
    x*=m;
    y*=m;
    z*=m;
  }

  //distance (to this vector) from calling vector)
  float distance(Vec v)
  {
    float d= sqrt(pow(v.x-x,2) + pow(v.y-y,2) + pow(v.z-z,2)); 
    return d;
  }

  //calculate the magnitude (length) of the vector and return the magnitude of the vector  
  float magnitude()
  {
    return (float) sqrt(pow(x,2) + pow(y,2) + pow(z,2) ); 
  }

  void rotateVec(float ang)
  {
    x=cos(ang);
    y=sin(ang);
  }

  void normalize()
  {
    x = x/(float) sqrt(pow(x,2) + pow(y,2) + pow(z,2) ); 
    y = y/(float) sqrt(pow(x,2) + pow(y,2) + pow(z,2) ); 
  }

  //set function values through manual input //change vector parameters without using another function 
  void set (float xx, float yy, float zz) 
  {  
    x=xx; 
    y=yy; 
    z=zz;  
  } 

  //set function values with vector values //change vector parameters without using another function
  void set (Vec v) 
  {  
    x=v.x; 
    y=v.y; 
    z=v.z;  
  }   
}
