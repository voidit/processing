class Vislines
{

  
  float x1,y1,x2,y2; // start and end coords
  color c; // stroke color
  int a; // alpha value
  

  Vislines(float X1, float Y1, float X2, float Y2, int A, color C)
  {
    x1=X1;
    y1=Y1;
    x2=X2;
    y2=Y2;
    c=C;
    a=A;
  }

void drawLine()
{
  stroke(c,a);
  line(x1,y1,x2,y2);
}

}
