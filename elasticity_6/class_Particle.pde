class Particle
{  
  //Vislines [] particleLines = new Vislines[500]; // array of moving attractors (adult)
  Vec partMoveDir = new Vec(0,0,0); // sets wandering vector when part/atr snap

  float w,h; // width and height
  float t=0; // translate smoothing
  color lineColor = color(0); // line color
  int lineAlpha = 66; // line alpha

  color c3= color  (130,255,25); //green
  color c4= color  (0,255,255); //blue

  boolean runOnce = true; // sets values at moment of particle to movingatr swap
  boolean forgetting =false; // whether particle is looking for static atr or not
  float xdirection = 1;  // Left or Right
  float ydirection = 1;  // Top to Bottom
  float accel=1; // acceleration value
  float xP, yP;  // start position
  float aoeAtr=200; // max distance to engage with attractor

  int maxStaticCount=3; // max # of static atr a particle will be attracted too
  int staticCounter=0; // # of static attractors within particles aoe
  int [] staticAttracted = new int[maxStaticCount]; // keeps track of which static atr particle is attracted too

  float partMoveX=0; // determines wandering vector
  float partMoveY=0; // ^^
  int partStart; // determines which screen edge particle starts on

  float ty,tx;
  float angle = 0.0;
  float speed = .33;

  Vec pos = new Vec(0,0,0); // current point position of particle NOT A VECTOR
  Vec v = new Vec(0,0,0); // current vector 

  Particle(float X, float Y, float W, float H, int _partStart) //constructor
  {
    pos.x=X;
    pos.y=Y;
    w=W;
    h=H;
    partStart=_partStart;

    /*for(int i=0; i<particleLines.length;i++) // give particleLine array null values
     {
     particleLines[i]=new Vislines(0,0,0,0,lineAlpha,lineColor); 
     }*/
  }



  //////////////////////////////////////
  void step(Attractor [] a, Attractor b) // when particle is following moving and engaging statics
  {
    float d1 = 0; // distance from static to particle
    float d2 = 0;  //distance from associated moving to particle
    float ax = 0;  
    float ay = 0; 

    for(int j=0; j<staticAttracted.length;j++) // give staticAttracted array null values
    {
      staticAttracted[j] = -1; 
    }

    ////////////////////// movement due to static attractor////////////////////////////
    for (int i=0;i<a.length;i++)  
    { 
      if(staticCounter<3) // only check for static attractors if particle is engaged with less than 2
      {
        d1=pos.distance(a[i].pos);
        if (d1 < aoeAtr/2 && a[i].numPart < 6 ) //if distance is within child particle aoe and less than X particles are engaged at that attractor
        { 
          a[i].numPart++;
          if(d1 > (aoeAtr/2)*.75) // if distance is 75% of aoe radius or more, slows down particle movement at first
          {
            pos.x +=(a[i].pos.x-pos.x) * d1/2000; 
            pos.y +=(a[i].pos.y-pos.y) * d1/2000;
          }
          else
          {
            pos.x += (a[i].pos.x-pos.x) * d1/(1000-a[i].numPart*50); // smaller divider number stronger force of attraction
            pos.y += (a[i].pos.y-pos.y) * d1/(1000-a[i].numPart*50); // as particles accumulate, force stronger
          }
          if (visualSwitch==true) 
          { 
            lineColor=color(0); 
            lineAlpha=33;
            stroke(lineColor, lineAlpha);
            line(a[i].pos.x, a[i].pos.y, pos.x, pos.y); // line to static
            //prepLineArray(a[i].pos.x, a[i].pos.y);
          }
          else if (visualSwitch==false) 
          { 
            stroke(0);
            line(pos.x,pos.y, a[i].pos.x, a[i].pos.y);
          }
          if(b.snapCounter > SNAP_TIME-1) staticAttracted[staticCounter] = i; // loads all engaged static atr's array position just before snap 
          staticCounter++;
        } 
      }
    }

    ////////////////////// movement due to moving attractor////////////////////////////
    d2=pos.distance(b.pos); // always calculate associated parent attractor after all statics checked
    if (d2 < b.aoeAtr/2) // condition 1x - if distance smaller than parent aoe
    {
      if (d2 < aoeAtr/2) // condition 1A - if d2 smaller than particle aoe
      {
        pos.x += (b.pos.x-pos.x) * d2/1000; // smaller divider number stronger force of attraction
        pos.y += (b.pos.y-pos.y) * d2/1000; 
        if(staticCounter==0) // if only engaged by moving attractor and no statics, sinusoidal movement
        {

          curveTrans(100/d2);
        }
        if (visualSwitch==true)
        {
          lineColor=lerpColor(b.c1,b.c2,d2/(aoeAtr/2));
          lineAlpha=80;
          stroke(lineColor, lineAlpha);
          line(b.pos.x, b.pos.y, pos.x, pos.y); 
          //prepLineArray(b.pos.x, b.pos.y);
        }
        else if (visualSwitch==false) 
        {
          stroke(b.c);
          line(pos.x,pos.y, b.pos.x, b.pos.y);
        }
      }
      else if(d2 > aoeAtr/2) // condition 1B - if d2 larger than particle aoe (only possible with national pairs)
      {
        if (visualSwitch==true)
        {
          lineColor=lerpColor(b.c2,c3,d2/(b.aoeAtr/2));
          lineAlpha=80;
          stroke(lineColor, lineAlpha);
          line(b.pos.x, b.pos.y, pos.x, pos.y);
          //prepLineArray(b.pos.x, b.pos.y);
        }
        else if (visualSwitch==false) 
        {
          stroke(0,130,255); 
          //line(pos.x,pos.y, b.pos.x, b.pos.y);
        }
      }
    }
    else if(d2 > b.aoeAtr/2) // condition 2x - if d2 outside of parent aoe
    {
      if(b.particle2moving==true) b.snapCounter++; // only increment when particle following moving
      if (d2 < aoeAtr/2) // condition 2A - if d2 smaller than child aoe (only possible with foreigner pairs)
      {
        pos.x += (b.pos.x-pos.x) * d2/1000; // smaller divider number stronger force of attraction
        pos.y += (b.pos.y-pos.y) * d2/1000; 
        if(staticCounter==0 && d2<aoeAtr/2 && forgetting==false) // if only engaged by moving attractor (parent), move somewhat freely
        {
          curveTrans(100/d2);
        }
        if (visualSwitch==true)
        {
          lineColor=lerpColor(b.c2,c3,d2/(b.aoeAtr/2));
          lineAlpha=80;
          stroke(lineColor, lineAlpha);
          line(b.pos.x, b.pos.y, pos.x, pos.y);
          //prepLineArray(b.pos.x, b.pos.y);
        }
        else if (visualSwitch==false) 
        {
          stroke(0,130,255);
          line(pos.x,pos.y, b.pos.x, b.pos.y);
        }
      }
      else if(d2 > aoeAtr/2) // condition 2B - if d2 larger than particle aoe (only possible with national pairs)
      {
        if (visualSwitch==true)
        {
          lineColor=lerpColor(c3,c3,d2/(b.aoeAtr/2));
          lineAlpha=80;
          stroke(lineColor, lineAlpha);
          line(b.pos.x, b.pos.y, pos.x, pos.y);
          //prepLineArray(b.pos.x, b.pos.y);
        }
        else if (visualSwitch==false) 
        {
          stroke(0,130,255); 
          line(pos.x,pos.y, b.pos.x, b.pos.y);
        }
      }
    }
    if (visualSwitch==true)
    {
      //for(int i=0; i<particleLines.length;i++) particleLines[i].drawLine(); // draw array of lines
    }
    staticCounter=0;
  }

  /////////////////////////////////////
  void setWanderVec() // wandering vector
  {
    while(partMoveX < .35 && partMoveX > -.3) partMoveX=random(-1,1); // ensures wandering vector is not too "slow"
    while(partMoveY < .35 && partMoveY > -.3) partMoveY=random(-1,1); // ^^
    partMoveDir = new Vec(partMoveX,partMoveY,0.0); // gives wandering particle new direction vector at moment of snapping
  }

  //////////////////////////////////////
  void wandering(Attractor c) // when particle wandering
  {
    float d3=pos.distance(c.pos); // always calculate associated particle/moving atr distance
    pos.x += partMoveDir.x*xdirection;
    pos.y += partMoveDir.y*ydirection;
    if (pos.x > width || pos.x < 0) {
      xdirection *= -1;
    }
    if (pos.y > height || pos.y < 0) {
      ydirection *= -1;
    } 
    if(d3>aoeAtr/2 && visualSwitch==true)
    { 
      stroke(255);
      strokeWeight(1);
      line(c.pos.x, c.pos.y, pos.x, pos.y);
    }
    if(d3<aoeAtr/2) // if moving atr back within aoe
    {
      if (visualSwitch==true)
      {
        lineColor=lerpColor(c3,c.c2,d3/(aoeAtr/2));
        lineAlpha=80;
        line(c.pos.x, c.pos.y, pos.x, pos.y);
        //prepLineArray(c.pos.x, c.pos.y);
      }
      else if (visualSwitch==false) 
      {
        stroke(255,255,0);
        line(pos.x,pos.y, c.pos.x, c.pos.y);
      }
    }
    if(d3<aoeAtr/6) // if within 1/3 of particle aoe radius reset relation
    {
      c.dir.x=partMoveDir.x; // sets moving atr vector to particle vector
      c.dir.y=partMoveDir.y;
      c.vel=random(1.5,2.0);
      partMoveX=0;
      partMoveY=0;
      forgetting=true; // sets particle to not look for statics
    }
    if (visualSwitch==true)
    {
      //for(int i=0; i<particleLines.length;i++) particleLines[i].drawLine(); // draw array of lines
    }
  }

  /* /////////////////////////////////////
   void prepLineArray(float x2, float y2)
   {
   for(int i=1;i<particleLines.length;i++) 
   {
   float fadeNumber=particleLines.length*.33; //% of line array fades out
   if(i < int(fadeNumber))
   {
   float alphaChange = 100/fadeNumber;  //amount to fade per increment
   float actualAlpha = i*alphaChange; // oldest line, i=1, faded the most to newest line, i=fadeNumber, the least
   particleLines[i].a = int(actualAlpha);
   }
   particleLines[i-1]=particleLines[i];
   }
   particleLines[particleLines.length-1]= new Vislines(pos.x, pos.y, x2, y2, lineAlpha, lineColor);
   }*/

  //////////////////////////////////////
  void curveTrans(float radius)
  { 
    angle+=speed;
    float sinval=sin(angle);
    float ty = sinval * radius;
    pos.y+=ty;
    float cosval=cos(angle);
    float tx = cosval * radius;
    pos.x+=tx;

  }

  //////////////////////////////////////
  void drawParticle()
  {
    strokeWeight(1);
    stroke(0,0,0,33);
    noFill();
    ellipse(pos.x, pos.y,aoeAtr,aoeAtr);
    noStroke();
    fill(0);
    ellipse(pos.x, pos.y,w,h);
  } 
}
