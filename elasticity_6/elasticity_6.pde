/* OpenProcessing Tweak of *@*http://www.openprocessing.org/sketch/285*@* */
/* !do not delete the line above, required for linking your tweak if you upload again */
//import processing.pdf.*;

int NUM_PAIRS=25; // number of (child) particle and (adult) moving attractors pairs
int NUM_STATIC=30; // number of static attractors
int SNAP_TIME=80; // # of frames until part/moving atr snap occurs
int FORGET_TIME=60; // # of frames particle "forgets" static atr 
int REBIRTH_TIME=60; // # frames until part/moving atr pair reset to start
boolean visualSwitch = true ; // swaps between behavior and resultant visualizations

float startLx, startLy; // left edge gate
float startBx, startBy; // bottom edge gate
float startRx, startRy; // right edge gate
float startTx, startTy; // top edge gate

Particle [] p = new Particle[NUM_PAIRS]; // array of particles (child)
Attractor [] movingAtr = new Attractor[NUM_PAIRS]; // array of moving attractors (adult)
Attractor [] staticAtr = new Attractor[NUM_STATIC]; // array of static attractors (non-adult)
Attractor [] nullAtr = new Attractor[0]; // array of null statics for particle in "forgetting" phase

Vec nullDir=new Vec(0,0,0); // a no movement vector for static attractors
Vec moveDir[]= new Vec[NUM_PAIRS]; // initial movement vector for moving attractors

int forgettingCounter=0; // used for how long particle "forgets" to look for static atr
int counter=0;

void setup()
{
  size(800,800);
  background(255);
  smooth();
  //beginRecord(PDF, "everything.pdf");

  setGateways(); // sets random start locations on each edge of screen
  setParticles(0, p.length); // creates particles in Particle class arreay
  setMoveAttractors(0, movingAtr.length); // creates movingattractors in Attractor class array
  setStaticAttractors(); // creates static attractos in Attractor class array

}

void draw()
{

  if(visualSwitch == false) background(255);
  else if(visualSwitch == true)
  {
    counter = counter+1; 
    int redrawer = counter%15; // every 4 frames 
    if (redrawer<=0) 
    { 
      fill(255,12); 
      noStroke(); 
      rect(0,0,width,height);   
    } 
  }

  for(int i=0; i<NUM_PAIRS; i++)
  {
    if(visualSwitch == false) p[i].drawParticle();
    if(movingAtr[i].particle2moving==true) // if particle following moving atr
    {
      movingAtr[i].stepAtr();
      p[i].step(staticAtr, movingAtr[i]);
    }
    else if(movingAtr[i].particle2moving==false) // if moving atr following particle
    {
      if(p[i].forgetting==false) // if particle is wandering
      {
        if(p[i].runOnce==true) // set initial wander vector only once
        {
          p[i].setWanderVec();
          p[i].runOnce=false; // wont run again until reset
        }
        movingAtr[i].snapCounter=0; // reset 
        movingAtr[i].attractAtr(p[i]); // moving atr to particle
        p[i].wandering(movingAtr[i]); // sets forgetting boolean
      }
      else if(p[i].forgetting==true) // if particle disregarding static attractors (forgetting)
      {
        movingAtr[i].stepAtr(); // moving atr normal
        p[i].step(nullAtr, movingAtr[i]); // all statics null to particle
        forgettingCounter++;
        if(forgettingCounter>FORGET_TIME) // number of frames in forgetting phase until particle/moving atr reset to initial condition
        {
          movingAtr[i].particle2moving=true; // particle following moving atr
          p[i].runOnce=true; // reset for when next snap occurs
          p[i].forgetting=false; // particle is looking for static atr
          forgettingCounter=0; // reset counter for next time
        }
      }
    }

    if(movingAtr[i].snapCounter > SNAP_TIME) // SNAP occurs after a X amount of time
    {
      if(visualSwitch==true) // snapping visual
      {
        movingAtr[i].drawSnap(i);
      }

      movingAtr[i].particle2moving=false; // moving atr follows particle now
      movingAtr[i].totalSnapCount++;
      for(int j=0; j<movingAtr.length; j++) // tests against all other moving atr
      {
        float d1 = movingAtr[i].pos.distance(movingAtr[j].pos);
        if(d1<movingAtr[i].aoeAtr/2) // if within moving atr aoe
        {
          if(j!=i && movingAtr[i].atrType==movingAtr[j].atrType) // update other same type of moving atr by fraction
          {
            movingAtr[j].updateStaticSnap(p[i].staticAttracted,.25); // updates moving atr's static atr deflection array

            if(visualSwitch==true)
            {
              fill(255,90);
              stroke(0,90);
              ellipse(movingAtr[j].pos.x, movingAtr[j].pos.y,15,15);
            }
          }
          else if(j==i) // update current moving atr fully
          {
            movingAtr[j].updateStaticSnap(p[i].staticAttracted,1.0); // updates moving atr's static atr deflection array
          }

        }
      }
    }

    if(movingAtr[i].rebornCounter>REBIRTH_TIME) //resets part/moving atr pair
    {
      setParticles(i, i+1); 
      setMoveAttractors(i, i+1);
    }

  }
  if(visualSwitch == false) drawAtr();
  for(int i=0; i<staticAtr.length; i++) staticAtr[i].numPart=0;
}

void setGateways() // start locations
{
  startLx=0; // left edge
  startLy=random(height*.1, height-(height*.1));

  startBx=random(width*.1, width-(width*.1)); // bottom edge
  startBy=height; 

  startRx=width; // right edge
  startRy=random(height*.1, height-(height*.1));

  startTx=random(width*.1, width-(width*.1)); // top edge
  startTy=0; 
}

void setParticles(int forStart, int forUntil)
{
  for(int i=0; i<p.length; i++) 
  {
    float partStart=random(4);
    if(partStart<1) partStart=0;
    if(partStart>=1 && partStart<2) partStart=1;
    if(partStart>=2 && partStart<3) partStart=2;
    if(partStart>=3 && partStart<4) partStart=3;

    switch(int(partStart))
    {
    case 0:
      p[i]= new Particle(startLx,startLy,6,6,0);
      break;
    case 1:
      p[i]= new Particle(startBx,startBy,6,6,1);
      break;
    case 2:
      p[i]= new Particle(startRx,startRy,6,6,2);
      break;
    case 3:
      p[i]= new Particle(startTx,startTy,6,6,3);
      break;
    }
  }
}

/*void keyPressed() 
{
  if (key == 'q') 
  {
    endRecord();
    exit();
  }
}*/

void setMoveAttractors(int forStart, int forUntil)
{
  float atrType=0; // 0 = national, 1 = foreigner, moving attractor, randomly decided in setup

  /////// MOVING
  for (int i=0; i<movingAtr.length; i++)
  {
    atrType=random(0,1); // randomly generate either foreign or national moving attractors
    if(atrType<=.5) atrType=0; 
    else atrType=1;

    switch(p[i].partStart)
    {
    case 0:
      moveDir[i] = new Vec(random(.5,2.0),random(-1.5,1.5),0.0); // random starting vector of movement;
      break;
    case 1:
      moveDir[i] = new Vec(random(-1.5,1.5),random(-2.0,-.5),0.0); // random starting vector of movement;
      break;
    case 2:
      moveDir[i] = new Vec(random(-2.0,-.5),random(-1.5,1.5),0.0); // random starting vector of movement;
      break;
    case 3:
      moveDir[i] = new Vec(random(-1.5,1.5),random(.5,2.0),0.0); // random starting vector of movement;
      break;
    }

    movingAtr[i]=new Attractor(p[i].pos.x, p[i].pos.y, moveDir[i], int(atrType), random(1.0,2.0));
  }
}

void setStaticAttractors()
{
  ///////// STATIC
  for (int i=0; i<staticAtr.length; i++)
  {
    staticAtr[i]=new Attractor(random(25,width-25), random(25,height-25), nullDir,2,0);
    for(int j=0; j<i; j++) // tests to make sure no statics are drawn within X distance of any other
    {
      if(staticAtr[i].pos.distance(staticAtr[j].pos) < 50) i--;
    }
  }
}

void drawAtr()
{
  for (int i=0; i<movingAtr.length; i++) movingAtr[i].drawAtr();
  for (int i=0; i<staticAtr.length; i++) staticAtr[i].drawAtr();
}

void mouseClicked() // true shows resultant visualization, false behavior visualization, swaps on mouseclick
{
  if(visualSwitch==true) visualSwitch=false;
  else if (visualSwitch==false) visualSwitch=true; 
  setup();
}
