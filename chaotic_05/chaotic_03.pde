float maxX = 2.8;
float minX = -2.8;
float maxY = 2;
float minY = -2;
float minZ = -1.2;
float maxZ = 1.2;
float x1 = 0;
float y1 = 0;
float z1 = 0;
float deltaX, deltaY, deltaZ, tempX, tempY, tempZ;
float row, col;
int dim;
int max_row = 240;
int max_col = 320;
int max_depth = 100;
color CL = color(217, 220, 163);
float a = 2.24;
float b = 0.43;
float c = -0.65;
float d = -2.43;
float e = 1;
int max_iterations = 50000, n;
BImage BGI;
color middle_grey = color(127, 127, 127, 50);
color grey = color(127, 127, 127);
color black = color(0, 0, 0, 90);
color white = color(255, 255, 255, 90);
/*------------------------------------------*/

size(max_col, max_row);
BGI = loadImage("Picture 2.jpg");
background(BGI);

//--------------------
color[] matrix_01 = new color[pixels.length];
//-----------------------
/* copying pixels to extra matrix */
for (int i = 0; i < (width * height); i++) {
  matrix_01[i] = pixels[i];
}
/* clear background with color */
background(CL);
//-----------------------
/* copying pixels from extra matrix */
//-----------------------
colorMode(RGB, 0.3);
smooth();

for (int i = 0; i < (width * height); i++) {
  int west = i - 1;
  int east = i + 1;
  if (pixels[i] > grey){
    pixels[i] += (middle_grey - matrix_01[i])/2;
    if (i > 0){
      pixels[west] += (middle_grey - matrix_01[west])/2;
    }
    if (i < (pixels.length - 1)){
      pixels[east] += (middle_grey - matrix_01[east])/2;
    }
  }else{
    pixels[i] -= (matrix_01[i] - middle_grey)/2;
    if (i > 0){
      pixels[west] -= ( matrix_01[west] - middle_grey)/2;
    }
    if (i < (pixels.length - 1)){
      pixels[east] -= (matrix_01[east] - middle_grey)/2;
    }
  }
}
//----------------------------

deltaX = max_col / (maxX - minX);
deltaY = max_row / (maxY - minY);
deltaZ = max_depth / (maxZ - minZ);
for (dim = 0; dim < 2; dim++){
  for (n = 0; n < max_iterations; n++){

    tempX = sin(a * y1) - z1 * cos(b * x1);
    tempY = z1 * sin(c * x1) - cos(d *y1);
    z1 = e * sin(x1);
    x1 = tempX;
    y1 = tempY;

    if (dim == 0){
      col = (x1 - minX) * deltaX;
      row = (y1 - minY) * deltaY;
    }
    else
    {
      col = (x1 - minX) * deltaX;
      row = (y1 - minY) * deltaY;
    }
    if ((col > 0) && (col <= max_col) && (row > 0) && (row <= max_row)){

      colorMode(RGB, 50);
      smooth();

      CL = get(int(col), int(row));
      if (CL/2 > grey){
        //CL += (CL - middle_grey)%CL;
        CL += middle_grey;
        //CL = white;
      }
      else
      {
        //CL -= (CL + middle_grey)%CL;
        CL -= middle_grey;
        //CL = black;
      }

      set(int(col), int(row), CL);
      //point(col, row, z1);
    }
  }
}
//print(CL);

