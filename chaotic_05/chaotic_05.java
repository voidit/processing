import java.applet.*; import java.awt.*; import java.awt.image.*; import java.awt.event.*; import java.io.*; import java.net.*; import java.text.*; import java.util.*; import java.util.zip.*; import netscape.javascript.*; import javax.comm.*; import javax.sound.midi.*; import javax.sound.midi.spi.*; import javax.sound.sampled.*; import javax.sound.sampled.spi.*; import javax.xml.parsers.*; import javax.xml.transform.*; import javax.xml.transform.dom.*; import javax.xml.transform.sax.*; import javax.xml.transform.stream.*; import org.xml.sax.*; import org.xml.sax.ext.*; import org.xml.sax.helpers.*;
public class chaotic_05 extends BApplet {
void setup() {
	size(320, 240);
}

void draw() {
	float maxX = 2.8f;
	float minX = -2.8f;
	float maxY = 2;
	float minY = -2;
	float minZ = -1.2f;
	float maxZ = 1.2f;
	float x1 = 0;
	float y1 = 0;
	float z1 = 0;
	float deltaX, deltaY, deltaZ, tempX, tempY, tempZ;
	float row, col;
	int dim;
	int max_row = 240;
	int max_col = 320;
	int max_depth = 100;
	int CL = color(217, 220, 163);
	float a = 2.24f;
	float b = 0.43f;
	float c = -0.65f;
	float d = -2.43f;
	float e = 1;
	int max_iterations = 50000, n;
	BImage BGI;
	int middle_grey = color(127, 127, 127, 50);
	int grey = color(127, 127, 127);
	int black = color(0, 0, 0, 90);
	int white = color(255, 255, 255, 90);
	/*------------------------------------------*/

	size(max_col, max_row);
	BGI = loadImage("Picture 2.jpg");
	background(BGI);

	//--------------------
	int[] matrix_01 = new int[pixels.length];
	//-----------------------
	/* copying pixels to extra matrix */
	for (int i = 0; i < (width * height); i++) {
		matrix_01[i] = pixels[i];
	}
	/* clear background with color */
	background(CL);
	//-----------------------

	/* copying pixels from extra matrix */
	colorMode(RGB, 0.3f);
	smooth();

for (int i = 0; i < (width * height); i++) {
	int west = i - 1;
	int east = i + 1;
	int north = i - width;
	int south = i + width;
	int current_col = i % width;
	int current_row = i % height;

	if (pixels[i] > grey){
		pixels[i] += (middle_grey - matrix_01[i])/2;
		/*
		if (i > north){
		pixels[north] += (middle_grey - matrix_01[i])/2;
		}
		if (i < south){
		pixels[south] += (middle_grey - matrix_01[i])/2;
		}
		*/
		if (i > 0){
			pixels[west] += (middle_grey - matrix_01[i])/2;
		}

		if (i < (pixels.length - 1)){
			pixels[east] += (middle_grey - matrix_01[i])/2;
		}

	}else{
		pixels[i] -= (matrix_01[i] + middle_grey)/2;

		if (i > 0){
			pixels[west] -= ( matrix_01[i] + middle_grey)/2;
		}

		if (i < (pixels.length - 1)){
			pixels[east] -= (matrix_01[i] + middle_grey)/2;
		}
	}
		//print (i & north & south);
}

	//----------------------------

	deltaX = max_col / (maxX - minX);
	deltaY = max_row / (maxY - minY);
	deltaZ = max_depth / (maxZ - minZ);

	for (dim = 0; dim < 2; dim++){
		for (n = 0; n < max_iterations; n++){

			tempX = sin(a * y1) - z1 * cos(b * x1);
			tempY = z1 * sin(c * x1) - cos(d *y1);
			z1 = e * sin(x1);
			x1 = tempX;
			y1 = tempY;

			if (dim == 0){
				col = (x1 - minX) * deltaX;
				row = (y1 - minY) * deltaY;
			}
			else
			{
				col = (x1 - minX) * deltaX;
				row = (y1 - minY) * deltaY;
			}

			if ((col > 0) && (col <= max_col) && (row > 0) && (row <= max_row)){
				colorMode(RGB, 50);
				smooth();

				CL = get((int)(col), (int)(row));
				if (CL/2 > grey){
				//CL += (CL - middle_grey)%CL;
				//CL += middle_grey;
				CL = (white + CL)/2;
			}
			else
			{
				//CL -= (CL + middle_grey)%CL;
				//CL -= middle_grey;
				CL = black;
			}
			set((int)(col), (int)(row), CL);
			//point(col, row, z1);
			}
		}
	}
	//print(CL);
	}
}