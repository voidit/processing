int outputLen = 700; // Define the length of the LiveOutput Stream.

float rad = 10.0, x, y, a;
float offsetX = 350.0;
float offsetY = 350.0;
int numCircles = 1, b;

void setup(){
  size(700, 700);
  background(255);
  Sonia.start(this,11000); // Start Sonia in a lower sampling rate, for improved CPU performance.
  LiveOutput.start(outputLen,outputLen*2); // Start LiveOutput, define the Stream-length and Stream-buffer.
LiveOutput.startStream(); // Start the streaming -activates the liveOutputEvent(){}
}

void loop(){
  background(223, 228, 194);
  for (a = 0.0; a < TWO_PI * numCircles; a += PI/360.0){
    //offsetX = a;
    //offsetY = a * 20.0;
    //offsetY += atan2(x, y)/100; //tube of circles -> eventually sounds like water
    //offsetY = tan(a) * rad / PI + 350; //draws strangely curved circle
    //offsetY = tan(a) * rad / TWO_PI + 350; //curly Q nice with changing rad

    //rad += sin(a/rad);  //spiral
    //rad -= tan(a/PI)*2.0; //just a sinewave with x++
    //rad -= tan(a/PI)*0.002; //very strange spiral something
    //rad -= tan(a/PI)*0.0002; //very strange spiral something: looks like fractals; implosion, and then explosion
    //rad += atan2(x, y)/10; //another spiral
    rad += atan2(cos(a), sin(a));
    if (x <= 1100.0){
      //offsetY += atan2(x, y)/1000; //tube of circles -> eventually sounds like water
      x += 0.01;
      //x = cos(a) * rad + offsetX
    } else {
      //x = -x;
      //offsetY = -offsetY*(atan2(x, y));
      x = cos(a) * rad + offsetX;
    }
    y = sin(a) * rad + offsetY;
    //x = cos(a += a) * rad + offsetX;
    //y = sin(a * a) * rad + offsetY;
    //x = pow(cos(a), 3) * rad + offsetX;
    //y = pow(sin(a), 3) * rad + offsetY;
    //y = sin(a += PI/10) * rad + offsetY; //just a sine with x++, beams of sines with rad changed otherwise
    //y = sin(a -= PI) * rad + offsetY; //stright line with x++

    //stroke(10);
    //point(x, y);
  }

  // draw the background graphics
  background(223, 228, 194);
  stroke(255, 217, 222);
  line(0,height/2, width,height/2);

  // Draw the waveform of the sample-data
  // notice that we multiply the data by 5, because it is originally in a float value range of 0.0 -> 1.0
  for (int i=0; i < outputLen-1; i++) {
    line(i,LiveOutput.data[i]*5 + height/2, i+1,LiveOutput.data[(i+1)]*5 + height/2);
  }
  //delay(int(b*x/y));  // Stops the program for b milliseconds
}
//--------------------------------------------------------
void liveOutputEvent(){

  float freq = x*y*1000; // determines the amount of sine-cycles to be created.
  float oneCycle = a; // math to be used in sin() below. it changes the value range, to produce 1 sin cycle.
  float amp = rad/100; // determines the amplitude of our sample data. (values range from 0.0 to 1.0);
  // populate the liveOutput 'data' array with sample data
  for (int i=0; i < outputLen; i++) {
    LiveOutput.data[i] = amp* sin(i*oneCycle*freq*a);
    // do a fade-out of the sample data by slowly 'shrinking' the array values.
    for (int ii=0; ii < outputLen; ii++) {
      for (b = 10; b >= 0; b--){
        LiveOutput.data[ii] *= 0.98;
      }
    }
  }
}

// Safely close the sound engine upon Browser shutdown.
public void stop(){
  LiveOutput.stopStream();
  Sonia.stop();
  super.stop();
}
