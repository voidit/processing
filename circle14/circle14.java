import com.*; import com.softsynth.*; import com.softsynth.jsyn.*; import com.softsynth.jsyn.view102.*; import com.softsynth.jsyn.util.*; import com.softsynth.jsyn.view11x.*; import com.softsynth.jsyn.view.*; import com.softsynth.jsyn.circuits.*; import com.softsynth.util.*; import com.softsynth.view.*; import com.softsynth.math.*; import com.*; import com.softsynth.*; import com.softsynth.jsyn.*; import java.applet.*; import java.awt.*; import java.awt.image.*; import java.awt.event.*; import java.io.*; import java.net.*; import java.text.*; import java.util.*; import java.util.zip.*; import netscape.javascript.*; import javax.comm.*; import javax.sound.midi.*; import javax.sound.midi.spi.*; import javax.sound.sampled.*; import javax.sound.sampled.spi.*; import javax.xml.parsers.*; import javax.xml.transform.*; import javax.xml.transform.dom.*; import javax.xml.transform.sax.*; import javax.xml.transform.stream.*; import org.xml.sax.*; import org.xml.sax.ext.*; import org.xml.sax.helpers.*; public class circle14 extends BApplet {int outputLen = 700; // Define the length of the LiveOutput Stream.

float rad = 10.0f, x, y, a;
float offsetX = 350.0f;
float offsetY = 350.0f;
int numCircles = 1, b;

void setup(){
  size(700, 700);
  background(255);
  Sonia.start(this,11000); // Start Sonia in a lower sampling rate, for improved CPU performance.
  LiveOutput.start(outputLen,outputLen*2); // Start LiveOutput, define the Stream-length and Stream-buffer.
LiveOutput.startStream(); // Start the streaming -activates the liveOutputEvent(){}
}

void loop(){
  for (a = 0.0f; a < TWO_PI * numCircles; a += PI/360.0f){
    //offsetX = a * 10.0;
    //offsetY = a * 20.0;
    //offsetY += atan2(x, y)/100; //tube of circles -> eventually sounds like water
    //offsetY = tan(a) * rad / PI + 350; //draws strangely curved circle
    //offsetY = tan(a) * rad / TWO_PI + 350; //curly Q nice with changing rad

    //rad += sin(a/rad);  //spiral
    //rad -= tan(a/PI)*2.0; //just a sinewave with x++
    //rad -= tan(a/PI)*0.002; //very strange spiral something
    rad += tan(a/PI)*0.0002f; //very strange spiral something: looks like fractals; implosion, and then explosion
    //rad += atan2(x, y)/10; //another spiral
    //rad += atan2(cos(a), sin(a));
    if (x <= 1100.0f){
      x += 0.01f;
    } else {
      x = -x;
    //}
    //x = cos(a) * rad + offsetX;
    LiveOutput.fireEvent();
    //twiddle of the sample data by reversing the array values.
    for (int ii=0; ii < outputLen; ii++) {
      for (b = outputLen; b <= 0; b--){
        LiveOutput.data[ii] = -LiveOutput.data[ii];
        print("  ."+LiveOutput.data[ii]);
      }
    }
  }
  y = sin(a) * rad + offsetY;
  //x = cos(a += a) * rad + offsetX;
  //y = sin(a * a) * rad + offsetY;
  //x = pow(cos(a), 3) * rad + offsetX;
  //y = pow(sin(a), 3) * rad + offsetY;
  //y = sin(a += PI/10) * rad + offsetY; //just a sine with x++, beams of sines with rad changed otherwise
  //y = sin(a -= PI) * rad + offsetY; //stright line with x++

  //stroke(10);
  //point(x, y);
}

// draw the background graphics
background(223, 228, 194);
stroke(255, 217, 222);
line(0,height/2, width,height/2);

// Draw the waveform of the sample-data
// notice that we multiply the data by 30, because it is originally in a float value range of 0.0 -> 1.0
for (int i=0; i < outputLen-1; i++) {
  line(i,LiveOutput.data[i]*30 + height/2, i+1,LiveOutput.data[(i+1)]*30 + height/2);
}

}
//--------------------------------------------------------
void liveOutputEvent(){

float freq = x*y/rad/b*100; // mouseX determines the amount of sine-cycles to be created.
float oneCycle = a; // math to be used in sin() below. it changes the value range, to produce 1 sin cycle.
float amp = rad * TWO_PI *a/outputLen; // determines the amplitude of our sample data. (values range from 0.0 to 1.0);

// populate the liveOutput 'data' array with sample data
for (int i=0; i < outputLen; i++) {
  LiveOutput.data[i] = amp* sin(i*oneCycle*freq);
  //do a fade-out of the sample data by slowly 'shrinking' the array values.
  for (int ii=0; ii < outputLen; ii++) {
    for (b = outputLen; b <= 0; b--){
      LiveOutput.data[ii] *= 0.98f;
    }
  }
}
}

// Safely close the sound engine upon Browser shutdown.
public void stop(){
LiveOutput.stopStream();
Sonia.stop();
super.stop();
}
}