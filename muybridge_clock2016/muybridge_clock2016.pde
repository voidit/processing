PFont f;
int tMin, tSec, tHour, tMS;
int offset, initSec;
boolean checkOffset;
PImage[] ajutant = new PImage[12];
String strName, strTime;

void setup(){
  size(400, 120);
  f = loadFont("Courier_New.vlw");

  for(int i = 0; i < ajutant.length; i++){
    strName = (i+1) + ".jpg";
    ajutant[i] = loadImage(strName);
    //print(strName);
  }
  textFont(f, 20);
  offset = 0;
  initSec = second();
  checkOffset = true;
}

void draw(){
  background(#000000);
  tHour = hour();
  tMin = minute();
  tSec = second() ;
  tMS = (millis()- offset)%999;
  image(ajutant[tHour % 12], 0, 0);
  image(ajutant[tMin % 12], 100, 0);
  image(ajutant[tSec % 12], (100 * 2), 0);
  image(ajutant[tMS % 12], (100 * 3), 0);

  if(checkOffset){
    if( initSec != tSec){
      offset = millis();
      checkOffset = false;
    }
  }

  strTime = nf(tHour,2) + " : " + nf(tMin,2) + " : " + nf(tSec,2) + " : " + nf(tMS,3) ;
  smooth();
  text(strTime, 1, 115);
  //print(strTime);
}