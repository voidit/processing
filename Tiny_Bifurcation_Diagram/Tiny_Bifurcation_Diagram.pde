/* OpenProcessing Tweak of *@*http://www.openprocessing.org/sketch/72995*@* */
/* !do not delete the line above, required for linking your tweak if you upload again */
/** 
A tiny sketch drawing the Bifurcation Diagram.<br>  
See also http://paulbourke.net/fractals/logistic/ */
int p=512;
void setup(){
 size(512,512, P2D); 
 stroke(254,4);
 background(0);
}
void draw()
{float n=0.5;
 float d=0.00002;
 for (float r=1+random(d); r<4; r=r+d)
 {n=r*n*(1-n);
  point(-61*r*(1-r)-220, p-n*p);
}}

void keyPressed()
{save("TBD.png");}

