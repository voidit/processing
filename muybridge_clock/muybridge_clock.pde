BFont f;
int tMin, tSec, tHour, tMS;
int offset, initSec;
boolean checkOffset;
BImage[] ajutant = new BImage[12];

void setup(){
  size(400, 120);
  f = loadFont("Courier_New.vlw");

  for(int i = 0; i < ajutant.length; i++){
    ajutant[i] = loadImage(String((i+1) + ".jpg"));
    //print(String((i+1) + ".jpg"));
  }
  textFont(f, 20);
  offset = 0;
  initSec = second();
  checkOffset = true;
}

void loop(){

  background(#595546);
  tHour = hour();
  tMin = minute();
  tSec = second() ;
  tMS = (millis()- offset)%999;
  image(ajutant[tHour % 12], 0, 0);
  image(ajutant[tMin % 12], 100, 0);
  image(ajutant[tSec % 12], (100 * 2), 0);
  image(ajutant[tMS % 12], (100 * 3), 0);

  if(checkOffset){
    if( initSec != tSec){
      offset = millis();
      checkOffset = false;
    }
  }

  String time = nf(tHour,2) + " : " + nf(tMin,2) + " : " + nf(tSec,2) + " : " + nf(tMS,3) ;
  smooth();
  text(time, 1, 115);
}

