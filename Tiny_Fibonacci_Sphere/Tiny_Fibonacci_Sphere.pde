/* OpenProcessing Tweak of *@*http://www.openprocessing.org/sketch/62322*@* */
/* !do not delete the line above, required for linking your tweak if you upload again */

// Tiny_Fibonacci_Sphere.pde 
// v1.0  5/2012
// v2.0  1/2016  (processing 3.0.1)
// tags: tiny, fibonacci, sphere

float rX=0.7, rY, vX, vY;
float phi=(sqrt(5)+1)*0.5-1; // golden ratio
int p=200;    // number of starting points

void setup() 
{ 
  size(600, 600, P3D);
  smooth();
  strokeWeight(13);  
  stroke(0, 127);
}

void draw()
{ 
  background(255);         
  fill(0);
  text(p, 20,20);
  translate(width/2.0, height/2.0, 0);
  vX = 0.95 * vX + (mouseY-pmouseY)*1.6E-4;  
  rX += vX;  
  rotateX(rX);  
  vY = 0.95 * vY + (mouseX-pmouseX)*1.6E-4;  
  rY += vY;  
  rotateY(rY);  
  if (!mousePressed) p += 1;
  strokeWeight(1+(2000.0+p)/p);
  for (int i = 1; i <= p; ++i)
  { 
    pushMatrix();
    rotateY((phi*i -floor(phi*i))*2.0*PI);
    rotateZ(asin(2*i/(float)p-1));
    stroke(0,0,phi*200,127);
    point(222.0, 0);
    popMatrix();
  }
  //println(int(frameRate "fps"));
}
void keyPressed()
{
  save("Tiny_Fibonacci_Sphere.png");
}