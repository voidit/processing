float maxX = 2.8;
float minX = -2.8;
float maxY = 2;
float minY = -2;
float minZ = -1.2;
float maxZ = 1.2;
float x1 = 0;
float y1 = 0;
float z1 = 0;
float deltaX, deltaY, deltaZ, tempX, tempY, tempZ;
float row, col;
int dim;
int max_row = 240;
int max_col = 320;
int max_depth = 100;
color CL = color(217, 220, 163);
float a = 2.24;
float b = 0.43;
float c = -0.65;
float d = -2.43;
float e = 1;
int max_iterations = 50000, n;
//---------------------------------------
BImage BGI;
BImage BGII;
color middle_grey = color(127, 127, 127, 50);
color grey = color(127, 127, 127);
color black = color(0, 0, 0, 90);
color white = color(255, 255, 255, 90);
/*------------------------------------------*/
size(max_col, max_row);
BGI = loadImage("Picture 1.jpg");
BGII = loadImage("Picture 2.jpg");
color[] matrix_01 = new color[BGI.pixels.length];
float[] matrix_02 = new float[BGI.pixels.length];
float[] matrix_03 = new float[BGI.pixels.length];
float[] matrix_04 = new float[BGI.pixels.length];
//-----------------------
/* clear background with color */
background(CL);
//-----------------------

/* copying pixels to extra matrix */
for (int i = 0; i < BGI.pixels.length; i++) {
  pixels[i] += (BGI.pixels[i] + BGII.pixels[i]) % CL;
  matrix_01[i] = BGI.pixels[i];
  matrix_03[i] = saturation(BGI.pixels[i]);
  matrix_04[i] = brightness(BGI.pixels[i]);
  matrix_02[i] = hue(BGII.pixels[i]);
}
//------------------------

deltaX = max_col / (maxX - minX);
deltaY = max_row / (maxY - minY);
deltaZ = max_depth / (maxZ - minZ);
for (dim = 0; dim < 2; dim++){
  for (n = 0; n < max_iterations; n++){

    tempX = sin(a * y1) - z1 * cos(b * x1);
    tempY = z1 * sin(c * x1) - cos(d *y1);
    z1 = e * sin(x1);
    x1 = tempX;
    y1 = tempY;

    if (dim == 0){
      col = (x1 - minX) * deltaX;
      row = (y1 - minY) * deltaY;
    }
    else
    {
      col = (x1 - minX) * deltaX;
      row = (y1 - minY) * deltaY;
    }
    if ((col > 0) && (col <= max_col) && (row > 0) && (row <= max_row)){

      int index = (int)(row * width + col);

      //-------------------
      /* copying pixels from extra matrix */
      colorMode(RGB, 0.5);
      smooth();
      
      pixels[index] += red(BGI.pixels[index]);
      pixels[index] += green(BGII.pixels[index]);
      pixels[index] += blue(BGII.pixels[index]);

      colorMode(HSB, 255);
      smooth();
      pixels[index] -= matrix_02[index];
      pixels[index] -= matrix_03[index];
      pixels[index] -= matrix_04[index];

      //------------------
      /* copying pixels from extra matrix */
      colorMode(RGB, 0.3);
      smooth();
      CL = matrix_01[index];
      //----------------------

      //CL = get(int(col), int(row));
      if (CL > grey){
        //CL += (CL + middle_grey)%CL;
        CL += middle_grey;
        //CL += (white + CL)/2;
      }
      else
      {
        CL = black;
        //CL -= (CL - middle_grey)%CL;
        CL -= middle_grey;
      }

      //set(int(col), int(row), CL);
      stroke(CL);
      fill(CL);
      point(col, row, z1);
    }
  }
}

