/* OpenProcessing Tweak of *@*http://www.openprocessing.org/sketch/221555*@* */
/* !do not delete the line above, required for linking your tweak if you upload again */
void setup(){
  background(0);    //Setting Background to Black
  //fullScreen();    //Setting Screen to Fullscreen
  size(800,600);    //Setting screen to 800,600
  strokeWeight(0);    //Setting Stroke to 0
  drawFib(1,0,1,width/2,height/2);    //Calling the Recursive Function drawFib with the arguments 1 for the starting number, 0 as the Previous Number, Starting Counter at 1 and x and y set to screen width and height
}
void drawFib(long a, long b, int step, int x, int y) {
  if (step<20) {    //See if counter is less than 20, if greater it will end the recursive loop
    fill(random(0,15)*17,random(0,15)*17,random(0,15)*17);    //Setting Fill to random 0-255 int that is a multiple of 17
    if (step==1) //Did not want to Modify X or Y here, would have broken the program, also would not have program originate from center
    arc(x,y,a*2,a*2,PI+QUARTER_PI,TWO_PI); //Makes an arc at X and Y with width and height of a*2, with an arc going from PI to 3PI/2      
    if (step%4 == 1 && step != 1)    //At the first step of every circulation that is not step #1 do the following
    arc((x-=a)+a,y+a,a*2,a*2,PI, PI+HALF_PI);    //Change the X to be x-a and then add a for this particular arc, add a to Y, these tranformations set the arc to have center at (0,a) being relative to the square\
    if (step%4 == 2)    //At the second step of every circulation do the following
    arc(x+a,y+=b,a*2,a*2,HALF_PI,PI);    //Add a to x for this particular arc, then change y to be y+b, setting the center to be at (a,0) relative to the square
    if (step%4 == 3)    //At the third step of every circulation do the following
    arc(x+=b,y-=(a-b),a*2,a*2,0,HALF_PI);    //Add b to x, then set y to be y-(a-b), setting center at (0,0)
    if (step%4 == 0)    //At the fourth step of every circulation do the following
    arc(x-=(a-b),(y-=a)+a,a*2,a*2,PI+HALF_PI,TWO_PI);    //Subtract a-b from x, do set y to be y-a and then for this particular arc add a setting center point to be (0,a)
    println(a);    //Print a to the console
    drawFib(a+b,a,step+1,x,y);    //Call the function so that new a is b(Which is the previous number) + a, then set previous number to be a, add the counter, pass x and y
  }
}