/* OpenProcessing Tweak of *@*http://www.openprocessing.org/sketch/1168*@* */
/* !do not delete the line above, required for linking your tweak if you upload again */
Graph graph;
PFont trebuchet;
float lengthMin = 1;
float lengthMax = 100;
float selectDistance = 15;
float rotationSpeed = PI/1000;

Vertex cur, begin, end;
Vector path = null;

void setup() {
  size(640, 640, P3D);
  colorMode(RGB, 1);
  
  trebuchet = loadFont("TrebuchetMS-48.vlw");
  textFont(trebuchet, 10);
  textAlign(CENTER);
  
  graph = loadTags("tags.txt");
  
  cur = begin = end = null;
}

void draw() {
  background(1);
  graph.render();
    
  if(begin != null && end != null)  { // render path
    for(int i = 1; i < path.size(); i++) {
      Vector3D lastp = ((Vertex) path.get(i-1)).particle.position();
      Vector3D curp = ((Vertex) path.get(i)).particle.position();
      stroke(0,0,.5,.6);
      line(lastp.x(), lastp.y(), lastp.z(), curp.x(), curp.y(), curp.z());
      ((Vertex) path.get(i)).light = true;
    }
    ((Vertex) path.lastElement()).light = false;
  }
  
  cur = graph.at();
  noStroke();
  if(cur != null) {
    fill(.5,.5);
    renderSelection(cur);
  }
  if(begin != null) {
    fill(1,.5,.5,.5);
    renderSelection(begin);
  }
  if(end != null) {
    fill(.5,1,.5,.5);
    renderSelection(end);
  }
}

void renderSelection(Vertex v) {
  Vector3D pos = v.particle.position();
  pushMatrix();
  translate(pos.x(), pos.y(), pos.z());
  rotateY(-frameCount*rotationSpeed);
  ellipse(0,0,selectDistance,selectDistance);
  popMatrix();
}

void mousePressed() {
  if(cur != null) {
    if(begin == null && cur != end)
      begin = cur;
    else if(begin == cur)
      begin = null;
    else if(end == null) {
      end = cur;
    } else if(end == cur)
      end = null;
    else
      end = cur;
    if(begin != null && end != null) { // compile path
      graph.resetMemos(); // memoization keep us from building a path
      graph.distance(begin, end);
      path = new Vector();
      Vertex inpath = end;
      path.add(inpath);
      while((inpath = inpath.previousVertex) != null)
        path.add(inpath);
    }
  }
}

void keyPressed() {
  setup();
}

Graph loadTags(String filename) {
  Graph graph = new Graph();
  String[] file = loadStrings(filename);
  for(int i = 0; i < file.length; i++) {
    String[] tags = subset(split(file[i], ' '), 1); // all but the first
    for(int j = 0; j < tags.length; j++) {
      graph.addVertex(tags[j]);
      for(int k = 0; k < tags.length; k++)
        if(j != k)
          graph.addEdge(tags[j], tags[k]);
    }
  }
  graph.setMedoids(3, 1, 5);
  return graph;
}
