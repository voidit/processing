// maps connected vertices to edges
class Vertex extends HashMap {
  // distance info
  float pathLength;
  Vertex previousVertex;
  
  // rendering properties
  boolean light;
  Particle particle;
  
  // essential properties
  int count;
  Object value;
  
  Vertex(Object value) {
    this.value = value;
    light = false;
    count = 0;
  }
  Edge getEdge(Vertex to, ForceDirectedGraph fdg) {
    Edge e = (Edge) get(to);
    if(e != null)
      return e;
    e = new Edge(this, to);
    e.spring = fdg.addEdge(particle, to.particle);
    put(to, e);
    return e;
  }
  void render(Graph parent) {    
    Vector3D xyz = particle.position();
    pushMatrix();
    translate(xyz.x(), xyz.y(), xyz.z());
    rotateY(-frameCount*rotationSpeed);
        
    float[] rgb = {
      parent.distance((Vertex) parent.medoids.get(0), this),
      parent.distance((Vertex) parent.medoids.get(1), this),
      parent.distance((Vertex) parent.medoids.get(2), this)
    };
    rgb = normalize(rgb);
    rgb = divide(rgb, 1.5);
    
    if(light) {
      fill(rgb[0], rgb[1], rgb[2], 0.5);
      ellipse(0,0,selectDistance,selectDistance);
      light = false;
    }
    
    fill(rgb[0], rgb[1], rgb[2], 0.9);
    textSize(count / 2 + 8);
    text(value.toString(),0,textAscent()/2);
    popMatrix();
    Vector E = new Vector(values());
    for(int i = 0; i < E.size(); i++)
      ((Edge) E.get(i)).render();
  }
  int hashCode() {
    return value.hashCode();
  }
  String toString() {
    return "v(" + count + "):" +
      value.toString();
  }
}

float[] normalize(float[] array) {
  float m = 0;
  for(int i = 0; i < array.length; i++)
    m = max(m, array[i]);
  float[] result = new float[array.length];
  for(int i = 0; i < array.length; i++)
    result[i] = array[i] / m;
  return result;
}

float[] divide(float[] array, float scalar) {
  float[] result = new float[array.length];
  for(int i = 0; i < array.length; i++)
    result[i] = array[i] / scalar;
  return result;
}
