import java.applet.*; import java.awt.*; import java.awt.image.*; import java.awt.event.*; import java.io.*; import java.net.*; import java.text.*; import java.util.*; import java.util.zip.*; import netscape.javascript.*; import javax.comm.*; import javax.sound.midi.*; import javax.sound.midi.spi.*; import javax.sound.sampled.*; import javax.sound.sampled.spi.*; import javax.xml.parsers.*; import javax.xml.transform.*; import javax.xml.transform.dom.*; import javax.xml.transform.sax.*; import javax.xml.transform.stream.*; import org.xml.sax.*; import org.xml.sax.ext.*; import org.xml.sax.helpers.*; public class chaotic_02 extends BApplet {void setup() { } void draw() {float maxX = 2.8f;
float minX = -2.8f;
float maxY = 2;
float minY = -2;
float minZ = -1.2f;
float maxZ = 1.2f;
float x1 = 0;
float y1 = 0;
float z1 = 0;
float deltaX, deltaY, deltaZ, tempX, tempY, tempZ;
float row, col;
int dim;
int max_row = 240;
int max_col = 320;
int max_depth = 100;
int CL = color(217, 220, 163);
float a = 2.24f;
float b = 0.43f;
float c = -0.65f;
float d = -2.43f;
float e = 1;
int max_iterations = 50000, n;
BImage BGI;
/*------------------------------------------*/

size(max_col, max_row);
BGI = loadImage("Picture 2.jpg");
background(BGI);
//background(CL);

deltaX = max_col / (maxX - minX);
deltaY = max_row / (maxY - minY);
deltaZ = max_depth / (maxZ - minZ);
for (dim = 0; dim < 2; dim++){
  for (n = 0; n < max_iterations; n++){

    tempX = sin(a * y1) - z1 * cos(b * x1);
    tempY = z1 * sin(c * x1) - cos(d *y1);
    z1 = e * sin(x1);
    x1 = tempX;
    y1 = tempY;

    if (dim == 0){
      col = (x1 - minX) * deltaX;
      row = (y1 - minY) * deltaY;
    }
    else
    {
      col = (x1 - minX) * deltaX;
      row = (y1 - minY) * deltaY;
    }
    if ((col > 0) && (col <= max_col) && (row > 0) && (row <= max_row)){
      CL = get((int)(col), (int)(row));
      int middle_grey = color(127, 127, 127);
      int black = color(0, 0, 0);
      int white = color(255, 255, 255);
      if (CL > middle_grey){
        CL += (CL - middle_grey)%CL;
        //CL += middle_grey;
        //CL = white;
      }
      else
      {
        CL -= (CL + middle_grey)%CL;
        //CL -= middle_grey;
        //CL = black;
      }
      set((int)(col), (int)(row), CL);
      //point(col, row, z1);
    }
  }
}
print(CL);

}}